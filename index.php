<?php
header("Content-Type: Text/Html;Charset=UTF-8");

require "./vendor/autoload.php";
try {
    $tron_obj = new \think\Tron("TRm4JAXwoibitHmAEcVVjm9xeLtKnbamhp", "29341cf74a1ef16b360ff0f49da15eab061ae9c1e05889719470bea36f4bde07");
    // 查询TRX余额
    //$result = $tron_obj->getBalance("TMYZmpS7QuWKEsKAVM5ZaeuES3PzoU3Px9");

    // 获取合约
    //$result = $tron_obj->getContract("TN9gUN4DBDed9aht7xqsnVCEcHr9eXpMSW");

    // 查询代币余额
    //$result = $tron_obj->getAccountToToken("TFhq2bUNFPmzRFtcasDychxK51q2Lznj8r", "TRm4JAXwoibitHmAEcVVjm9xeLtKnbamhp");

    // 地址转Hex
    //$result = $tron_obj->getAddressToHex("TYEaaHV4B7tVBwjYqkSVgP4jKQfDyFxvFY");

    // 代币转帐
    $result = $tron_obj->transferToToken("TFhq2bUNFPmzRFtcasDychxK51q2Lznj8r", "TAf1bijtoUFPoFm21V8fiCS2xPCvT7ApBL", "3");

    // TRX转账
    //$result = $tron_obj->trxTransaction("TRqo6jHXPeykyT6f9b5GehFww5iE223AEZ", 1);

    // 验证地址
    //$result = $tron_obj->validateAddress("TRqo6jHXPeykyT6f9b5GehFww5iE223AEZ");

    // 创建地址
    //$result = $tron_obj->generateAddress();

    // 生成地址
    //$result = $tron_obj->createAddress("BTC");

    // 根据私钥获取地址
    //$result = $tron_obj->getAddressByPrivateKey("df3da78d80a4583c2a9d839196449c0f210e7872161b649a1f9329df8c436b85");

    // 查上链上数据
    // $result = $tron_obj->getBlockByNum(25178509);

    // 查询交易流水
    //$result= $tron_obj->getTransaction("7b99fc891eda353b31a8e01781c9de7bb95009a90da5deb6724834e3ffbe80ce");

    // 查询交易流水费用
    //$result= $tron_obj->getTransactionInfo("7b99fc891eda353b31a8e01781c9de7bb95009a90da5deb6724834e3ffbe80ce");

    // 转换
    //$result = $tron_obj->fromTronExt(150000);

    // 获取合约
    //$result = $tron_obj->getContract("TFhq2bUNFPmzRFtcasDychxK51q2Lznj8r");

    // 获取当前区块
    //$result = $tron_obj->getCurrentBlock();

    // 获取区块高度
    //$result = $tron_obj->getBlockByNumber(25377547);
} catch (Exception $e) {
    $result = [
        "msg" => $e->getMessage()
    ];
}

echo "<pre/>";
print_r($result);
exit;

